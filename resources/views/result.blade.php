@extends ('welcome')
@section ('content')



    <form name="begin" method="post" action="/potencial">
        @csrf
        <p>Количество поставщиков(фирм) :{{$firm}}</p>
        <p>Количество магазинов :{{$magaz}}</p>
        <p>Количество складов :{{$sklad}}</p>
        <input type="hidden" name="firm" value="{{$firm}}">
        <input type="hidden" name="magaz" value="{{$magaz}}">
        <input type="hidden" name="sklad" value="{{$sklad}}">
        <h2>Начальное решение методом северо-западного угла:</h2>
        <table border="1">
            <thead>
            <tr>
                <td rowspan="2">Склады</td>
                <td colspan="{{$firm}}" align="Center">Поставщики</td>
                <td colspan="{{$magaz}}" align="Center">Потребители</td>
                <td rowspan="2">Поставки</td>
            </tr>
            <tr>
                @for($fir = 0 ; $fir< $firm ; $fir++)
                    <td>Фирма №{{$fir+1}}</td>
                @endfor
                @for($mag = 0 ; $mag< $magaz ; $mag++)
                    <td>Потребитель №{{$mag+1}}</td>
                @endfor

            </tr>
            </thead>
            <tbody>
            @for($skl = 0 ; $skl< $sklad ; $skl++)
                <tr>
                    <td rowspan="2">Cклад №{{$skl+1}}</td>
                    @for($fir = 0 ; $fir< $firm ; $fir++)
                        @if ($tovar[$skl][$fir] != 0)
                            <td style="background-color: darkgrey" align="right"><input type="text" size="2"  align="right" style="text-align: right" name="Czena[]" value="{{$czena[$skl][$fir]}}" readonly></td>
                        @else
                            <td align="right"><input type="text" size="2"  align="right" style="text-align: right" name="Czena[]" value="{{$czena[$skl][$fir]}}" readonly></td>
                        @endif
                    @endfor
                    @for($mag = 0 ; $mag< $magaz ; $mag++)
                        @if ($tovar[$skl][$firm+$mag] != 0)
                            <td style="background-color: darkgrey" align="right"><input type="text" size="2" align="right" style="text-align: right" name="Czena[]" value="{{$czena[$skl][$firm+$mag]}}" readonly></td>
                        @else
                            <td align="right"><input type="text" size="2" align="right" style="text-align: right" name="Czena[]" value="{{$czena[$skl][$firm+$mag]}}" readonly></td>
                        @endif
                    @endfor
                    <td rowspan="2"> <input type="text" size="5" align="center" name="postav[]" value="{{$postav[$skl]}}"></td>
                </tr>
                <tr>
                    @for($fir = 0 ; $fir< $firm ; $fir++)
                        @if ($tovar[$skl][$fir] != 0)
                            <td style="background-color: darkgrey"><input type="text" size="3"  align="center" name="tovar[]" value="{{$tovar[$skl][$fir]}}" readonly></td>
                        @else
                            <td><input type="text" size="3"  align="center" name="tovar[]" value="{{$tovar[$skl][$fir]}}" readonly></td>
                        @endif
                    @endfor
                    @for($mag = 0 ; $mag< $magaz ; $mag++)
                        @if ($tovar[$skl][$firm+$mag] != 0)
                            <td style="background-color: darkgrey"><input type="text" size="3"  align="center" name="tovar[]" value="{{$tovar[$skl][$firm+$mag]}}" readonly></td>
                        @else
                            <td><input type="text" size="3" align="center" name="tovar[]" value="{{$tovar[$skl][$firm+$mag]}}" readonly></td>
                        @endif
                    @endfor

                </tr>
            @endfor
            <tr>
            <tr align="center">
                <td>Потребление</td>
                @for($a = 0 ; $a < $firm+$magaz ; $a++)
                    <td colspan=""><input type="text" size="5" style="text-align: center" name="potreb[]" align="center" value="{{$potreb[$a]}}" readonly ></td>
                @endfor
            </tr>
            </tr>
            </tbody>
        </table>
        <p><input type="submit" value="Отправить">
            <input type="reset" value="Очистить"></p>
    </form>
@endsection
