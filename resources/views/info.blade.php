@extends ('welcome')
@section ('content')


<h2>Стоимость перевозки единицы продукции с фирм на склады :</h2>
    <form name="begin" method="post" action="/info">
        @csrf
        <p>Количество поставщиков(фирм) :{{$firm}}</p>
        <p>Количество магазинов :{{$magaz}}</p>
        <p>Количество складов :{{$sklad}}</p>
        <input type="hidden" name="firm" value="{{$firm}}">
        <input type="hidden" name="magaz" value="{{$magaz}}">
        <input type="hidden" name="sklad" value="{{$sklad}}">
        <table border="1">
            <thead>
<tr>
<td rowspan="2">Склады</td>
    <td colspan="{{$firm}}" align="Center">Поставщики</td>
    <td colspan="{{$magaz}}" align="Center">Потребители</td>
    <td rowspan="2">Поставки</td>
</tr>
<tr>
    @for($fir = 0 ; $fir< $firm ; $fir++)
    <td>Фирма №{{$fir+1}}</td>
        @endfor
        @for($mag = 0 ; $mag< $magaz ; $mag++)
            <td>Потребитель №{{$mag+1}}</td>
        @endfor

</tr>
            </thead>
            <tbody>
            @for($skl = 0 ; $skl< $sklad ; $skl++)
            <tr>
                <td rowspan="2">Cклад №{{$skl+1}}</td>
                @for($fir = 0 ; $fir< $firm ; $fir++)
                    <td align="right"><input type="text" size="2"  align="right" name="Czena[]" ></td>
                @endfor
                @for($mag = 0 ; $mag< $magaz ; $mag++)
                    <td align="right"><input type="text" size="2" align="right" name="Czena[]"></td>
                @endfor
                <td rowspan="2"> <input type="text" size="5" align="center" name="postav[]"></td>
            </tr>
                <tr>
                    @for($fir = 0 ; $fir< $firm ; $fir++)
                        <td><input type="text" size="3"  align="center" disabled></td>
                    @endfor
                    @for($mag = 0 ; $mag< $magaz ; $mag++)
                        <td><input type="text" size="3" align="center" disabled></td>
                    @endfor

                </tr>
            @endfor
            <tr>
            <tr align="center">
                <td>Потребление</td>
                @for($a = 0 ; $a < $firm+$magaz ; $a++)
                    <td colspan=""><input type="text" size="5" name="potreb[]" align="center"></td>
                @endfor
            </tr>
            </tr>
            </tbody>
        </table>
        <p><input type="submit" value="Отправить">
            <input type="reset" value="Очистить"></p>
    </form>
    @endsection
