@extends('welcome')
@section('content')
    <h2>Решение:</h2>

    <form name="begin" method="post" action="/result">
        @csrf
        <p>Количество поставщиков(фирм) :{{$firm}}</p>
        <p>Количество магазинов :{{$magaz}}</p>
        <p>Количество складов :{{$sklad}}</p>
        <input type="hidden" name="firm" value="{{$firm}}">
        <input type="hidden" name="magaz" value="{{$magaz}}">
        <input type="hidden" name="sklad" value="{{$sklad}}">
        <h2>Матрица Стоимость перевозки ед. продукции от фирмы на склад:</h2>
        <table border="1">
                @for ($fir= 0 ; $fir < $firm ; $fir++)
                    <tr>
                        @for($skl= 0 ; $skl < $sklad ; $skl++)
                            <td align="center"><input type="text" size="6" name="firm_sklad[]"  value="{{$firm_sklad[$fir][$skl]}}" readonly=""></td>
                            @endfor
                    </tr>
                    @endfor
        </table>
        <h2>Матрица Стоимость перевозки ед. продукции со склада в магазин:</h2>
        <table border="1">
            @for($skl= 0 ; $skl < $sklad ; $skl++)
                <tr>
                    @for ($mag = 0 ; $mag < $magaz ; $mag++)
                        <td align="center"><input type="text" size="6" name="sklad_magaz[]"  value="{{$sklad_magaz[$skl][$mag]}}" readonly=""></td>
                        @endfor
                </tr>
                @endfor
        </table>

        <h2>Матрица стоимостей  доставки с фирмы в магазины + фиктивный потр:</h2>
        <table border="1">
            <thead>
            <td>Фирмы</td>
            @for($mag = 1 ; $mag <=$magaz; $mag++)
                <td> Магазин № {{$mag}}</td>
            @endfor

            </thead>
            <tbody>
            @for($fir = 0; $fir < $firm ; $fir ++)
                <tr>
                    <td>Фирма № {{$fir+1}}</td>
                    @for($mag = 0 ; $mag < $magaz; $mag++)
                        <td align="center"><input type="text" size="6" name="firm_magaz[]"  value="{{$firm_magaz[$fir][$mag]}}" readonly=""></td>
                    @endfor
                </tr>
            @endfor
            </tbody>
        </table>

        <h2> Матрица стоимостей  доставки со Склада в Склад</h2>

            <table border="1">
                <thead>
                <tr>
                    <td>
                        Склады
                    </td>
                    @for($skl=1 ; $skl <= $sklad ; $skl++)
                        <td>
                            Склад № {{$skl}}
                        </td>
                    @endfor
                </tr>
                </thead>
                <tbody>
                @for($skl=0 ; $skl < $sklad ; $skl++)
                    <tr>
                        <td>Склад № {{$skl+1}}</td>
                        @for($sk=0 ; $sk < $sklad ; $sk++)
                                <td align="center"><input type="text" size="6" name="sklad_sklad[]"  value="{{$sklad_sklad[$skl][$sk]}}" readonly=""></td>
                        @endfor
                    </tr>
                @endfor
                </tbody>
            </table>

        <h2> Матрица Производства Фирм (А)</h2>
              <table border="1">
                    <tr>
                        @for($fir=0 ; $fir<$firm ; $fir++)
                            <td align="center"><input type="text" size="6" name="A[]"  value="{{$Obem[$fir]}}" readonly=""></td>
                            @endfor
                    </tr>
              </table>

        <h2> Матрица Емкости складов (D)</h2>
        <table border="1">
            <tr>
                @for($skl=0 ; $skl<$sklad ; $skl++)
                    <td align="center"><input type="text" size="6" name="D[]"  value="{{$emk[$skl]}}" readonly=""></td>
                @endfor
            </tr>
        </table>

        <h2> Матрица Потребностей Магазинов (B)</h2>
        <table border="1">
            <tr>
                @for($mag=0 ; $mag<$magaz ; $mag++)
                    <td align="center"><input type="text" size="6" name="B[]"  value="{{$potreb[$mag]}}" readonly=""></td>
                @endfor
            </tr>
        </table>

        <h2> Матрица Производства  (А1)</h2>
        <table border="1">
            <tr>
                @for($fir=0 ; $fir<$firm ; $fir++)
                    <td align="center"><input type="text" size="6" name="A1[]"  value="{{$Obem[$fir]}}" readonly=""></td>
                @endfor
                    @for($skl=0 ; $skl<$sklad ; $skl++)
                        <td align="center"><input type="text" size="6" name="A1[]"  value="{{$emk[$skl]}}" readonly=""></td>
                    @endfor
            </tr>
        </table>

        <h2> Матрица Потребностей (B1)</h2>
        <table border="1">
            <tr>
                @for($skl=0 ; $skl<$sklad ; $skl++)
                    <td align="center"><input type="text" size="6" name="B1[]"  value="{{$emk[$skl]}}" readonly=""></td>
                @endfor
                @for($mag=0 ; $mag<$magaz ; $mag++)
                    <td align="center"><input type="text" size="6" name="B1[]"  value="{{$potreb[$mag]}}" readonly=""></td>
                @endfor

            </tr>
        </table>
        <p><input type="submit" value="Отправить">
            <input type="reset" value="Очистить"></p>
    @endsection
